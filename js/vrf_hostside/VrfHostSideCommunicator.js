/**
 * http://usejsdoc.org/
 */

var aspRatio = 16.0/9.0;
var canvasContainerWidth = 0;
var canvasContainerHeight = 0;

//var vrfContainerSelector = '#fullContainer';
var vrfContainerSelector = '#showcase_viewdiv';
// function OnTemplateSiteIsFullyLoaded(){
// 	console.log("Template site is fully loaded.");
// // console.log("This process can be done more easily but just making a html,");
// // console.log("but made a detour to make dev process more resilliant...");
	
// 	recieveMessageHost();// add callback
// }
// 

$(document).ready(function(){

	console.log('HostsideCommunicator loaded');
	
	GetCanvasContainerDimension();
	
	//gq
	recieveMessageHost();// add callback
})

window.onresize = function(event) {
	GetCanvasContainerDimension();
	VrfCanvasResize(canvasContainerWidth, canvasContainerHeight);
};

function GetCanvasContainerDimension(){
	//image size
	var imgContainer = $(vrfContainerSelector);
	console.log("canvasContainer instances cnt : "+imgContainer.length);
	
	canvasContainerWidth = imgContainer.width();
	canvasContainerHeight = imgContainer.height();
}


// @ref
// https://medium.com/@jinro4/%EA%B0%9C%EB%B0%9C-window-postmessage%EB%A5%BC-%EC%9D%B4%EC%9A%A9%ED%95%9C-iframe-%ED%86%B5%EC%8B%A0-a542b8536518#.i59hco6gj
function postMessageFromHost(data, target = "*") {
	var wnd = $(vrfContainerSelector).find(".vrfIframe")[0].contentWindow;
	var stringified = JSON.stringify(data);
    return data && !isRestrictIEVersionHost(7) // IE7 이하는 지원 안함
       ? wnd.postMessage(stringified, target) 
       : false;
}
function postMessageFunctionFromHost(fnName,fnParam={}){
	postMessageFromHost({
		name : fnName,
		param : fnParam,
	});
} 
function isRestrictIEVersionHost(num){
	return false;
}
function recieveMessageHost() {
	console.log("message listener from hosting site is added");
	window.addEventListener('message', OnReceiveMessageListenerHost, false);
    return;
}
function OnReceiveMessageListenerHost(ev){
	var data = ev.data;
	console.log("host received : "+data);
    if (!data || !data.match(/^{.*}$/g)) {// Should be Json obj
        return;
    }
    var values = JSON.parse(data);

    var fnName = values.name;
    var fnParam = values.param; 
    OnReceiveMessageBehaviourHost(fnName,fnParam);
  
}

/*
 * fn to child iframe
 */
function OnReceiveMessageBehaviourHost(fnName,fnParam){
//    switch(fnName){
//    case "VrfCanvasLoaded":
//    	VrfCanvasLoaded(fnParam);
//    	break;
//    case 
//    }
	
	
	//https://www.sitepoint.com/call-javascript-function-string-without-using-eval/
	var fn = window[fnName];
	var targetObj = null;
	if (typeof fn === "function"){
		console.log("Function["+fnName+"] is called with param["+JSON.stringify(fnParam) +"]");
		//fn.apply(window, fnParam);
		setTimeout(function(){
			fn(fnParam);
		},0);
	}else{
		console.log("Message is ignored since it couldnt find ["+fnName+"] function");
	}
}

function VrfCanvasLoaded(param){
	console.log("VrfCanvasLoaded");
	
	// set layout
	// and resize canvas
	// 
	
	VrfCanvasResize(canvasContainerWidth, canvasContainerHeight)
	 
//	 var wid = canvasContainerWidth;
////	 var hei = wid / aspRatio;
//	 var hei = canvasContainerHeight;
//	 var vrfIframe = $(vrfContainerSelector).find(".vrfIframe")[0];
//	 vrfIframe.width = wid;
//	 vrfIframe.height = hei;
//	 vrfIframe.setAttribute('frameBorder',"0");
//	 vrfIframe.setAttribute('scrolling',"no");
//	 postMessageFromHost({
//	 	name : "SetCanvasSize",
//	 	param : {
//	 		width : wid,
//	 		height : hei,
//	 	}
//	 });
}

function VrfCanvasResize(wid, hei){
	 var vrfIframe = $(vrfContainerSelector).find(".vrfIframe")[0];
	 vrfIframe.width = wid;
	 vrfIframe.height = hei;
	 vrfIframe.setAttribute('frameBorder',"0");
	 vrfIframe.setAttribute('scrolling',"no");
	 postMessageFromHost({
	 	name : "SetCanvasSize",
	 	param : {
	 		width : wid,
	 		height : hei,
	 	}
	 });
}
/*******************************
 * Connection
 *******************************/
function OnUnitySceneLoaded(param){
	console.log("OnUnitySceneLoaded");
	
	//scroll automatically on unity canvas load
//	$('html,body').animate({ scrollTop: $("#navgroupTitle").offset().top }, 500);
}
function HtmlHeartbeat(msg){
	console.log("[Host]Received HtmlHeartbeat with msg "+msg);
}

/*******************************
 * Character
 *******************************/
function HtmlSetPosMinmax(area){
	console.log('HtmlSetPosMinmax' + JSON.stringify(area));
	console.log("SetCharControllableRect called with param : "+ JSON.stringify(area));
	
	var xStr = "X Range : " + area.xmin + " ~ " + area.xmax;
	var yStr = "Y Range : " + area.ymin + " ~ " + area.ymax;
	
	$('#strXrange').text(xStr);
	$('#strYrange').text(yStr);
}

function HtmlSetCharPose(pose){
	// var pose = {
	// x : posx,
	// y : posy,
	// rot : roty,
	// }
	SetInputgroupTextIfAvailable($('#inputboxX'), pose.x);
	SetInputgroupTextIfAvailable($('#inputboxY'), pose.y);
	SetInputgroupTextIfAvailable($('#inputboxRot'), pose.rot);
//	$('#inputboxX').find(':text').val(pose.x);
//	$('#inputboxY').find(':text').val(pose.y);
//	$('#inputboxRot').find(':text').val(pose.rot);
}
/*******************************
 * Camera
 *******************************/
function HtmlSetCamMode(camMode){
	console.log("[Host]HtmlSetCamMode called with param : "+camMode);
	//param : ThirdPerson, FirstPerson,
	switch(camMode.mode){
	case "ThirdPerson":
		$('#btnCamThirdPerson').closest('.btn').button('toggle');
		break;
	case "FirstPerson":
		$('#btnCamFirstPerson').closest('.btn').button('toggle');
		break;
	}
}

/*******************************
 * Corp
 *******************************/
function HtmlOnNotifyCorpInfos(param){
//	{corpIds:corpIds,corpNames:corpNames,}
	var corpIds = param.corpIds;
	var corpNames = param.corpNames;
	
	var len = corpIds.length;
	
	for (i = 0; i < len; i++) {
		var id = corpIds[i];
		var name = corpNames[i];
	    console.log("[Host]"+"Corps : Idx["+i+"] " +"Id["+id+"] " + "Name["+name+"] ");
	}
}

function HtmlSetCurrCorp(corp){
//	var corp = {
//		id:corpId,
//		name:corpName,
//	}
	$('#inputboxCorpId').find(':text').val(corp.id);
	$('#inputboxCorpName').find(':text').val(corp.name);
}

/*******************************
 * Pano
 *******************************/
function HtmlSetPanoInfo(panoInfo){
//	panoInfo = {
//			strTime:strTime,
//			doubleTime:doubleTime,
//			panoWid:panoWid,
//			panoHei:panoHei,
//			note:note,
//	}
//	$('#inputboxPanoTimeStr').find(':text').val(panoInfo.strTime);
//	$('#inputboxPanoTimeEpoch').find(':text').val(panoInfo.doubleTime);
//	$('#inputboxPanoPos').find(':text').val(panoInfo.note);
	
	var txt = panoInfo.strTime + "\n" + "(Wid:"+ panoInfo.panoWid + ",Hei:" + panoInfo.panoHei + ")";
	$("#txtPanoInfo").text(txt);
	//var txt = sprintf("Now you are at...\n%s", prop.hoverStr);
	//#('#txtPropInfo').text(txt);
}

/*******************************
 * Prop
 *******************************/
function HtmlSetCurrProp(prop){
//	var prop = {
//			propId:propId,
//			devNote:devNote,
//			hoverStr:hoverStr,
//			teleportable:teleportable,
//	}
	console.log("[Host]"+"HtmlSetCurrProp called with param "+JSON.stringify(prop));
	
//	$('#inputboxPropId').find(':text').val(prop.propId);
//	$('#inputboxPropDevnote').find(':text').val(prop.devNote);
//	$('#inputboxPropName').find(':text').val(prop.hoverStr);
	
	var txt = "Visited : " + prop.hoverStr;
	$("#txtPropInfo").text(txt);
	
}

function HtmlOnClickPropWithId(propId){
//	var propId = {
//			id:strId,
//	}
	console.log("[Host]"+"HtmlOnClickPropWithId called with param "+JSON.stringify(propId));
	console.log("PLEASE ADD SOMETHING TO HERE!!");
	
	$("#mediaScrollableModal").modal();
}

/*******************************
 * Util
 *******************************/
function SetInputgroupTextIfAvailable(target, txt){
	if (target.find(':text').is(":focus")) {
		console.log("The one with focused will not be updated- was setting text to ["+txt+"]");
	}else{
		target.find(':text').val(txt);	
	}	
}

function GetInputgroupText(target){
	return target.find(':text')[0].value;
}





