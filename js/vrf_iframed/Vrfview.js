/**
 * http://usejsdoc.org/
 */
$(document).ready(function(){
	console.log("Vrfview document is loaded");
	
// $('#testBtn')[0].onclick = function(){
// console.log("on testbutton clicked..");
// console.log("sending post to iframe holder");
//		
// var objTransfer = {
// data : "laksndflkassd",
// a : 12,
// b : "mac"
// };
// postMessageFromVrf({a:"meh"},"*");
// };	
	recieveMessageVrf();// receives message from hosting website

	var canvas = $('.emscripten#canvas');
	canvas.ready(function(){
		console.log("Vrief Canvas is loaded.");
		
		postMessageFromVrf({
			name : "VrfCanvasLoaded",
			param : {
			}
		});
	});
	

	
// var sampleIframeTagPath = "AutogenVrfviewSample/vrfviewSample.html";
// $('#vrfviewDiv').load(sampleIframeTagPath, function(){
// console.log("loaded : "+sampleIframeTagPath);
// });
});

/*******************************************************************************
 * messaging between host site and vrfview
 ******************************************************************************/
// @ref
// https://medium.com/@jinro4/%EA%B0%9C%EB%B0%9C-window-postmessage%EB%A5%BC-%EC%9D%B4%EC%9A%A9%ED%95%9C-iframe-%ED%86%B5%EC%8B%A0-a542b8536518#.i59hco6gj
function postMessageFromVrf(data, target = "*") {
	var stringified = JSON.stringify(data);
    return data && !isRestrictIEVersionVrf(7) // IE7 이하는 지원 안함
       ? window.parent.postMessage(stringified, target) 
       : false;
}
function postMessageFunctionFromVrf(fnName,fnParam={}){
	postMessageFromVrf({
		name : fnName,
		param : fnParam,
	});
} 
function isRestrictIEVersionVrf(num){
	return false;
}
function recieveMessageVrf() {
	console.log("message listener from Vrfing site is added");
	window.addEventListener('message', OnReceiveMessageListenerVrf, false);
    return;
}
function OnReceiveMessageListenerVrf(ev){
	var data = ev.data;
	console.log("Vrf received : "+data);
    if (!data || !data.match(/^{.*}$/g)) {// Should be Json obj
        return;
    }
    var values = JSON.parse(data);

    var fnName = values.name;
    var fnParam = values.param; 
    OnReceiveMessageBehaviourVrf(fnName,fnParam);
}

/*
 * fn to parent
 */
function OnReceiveMessageBehaviourVrf(fnName,fnParam){
	 // gq functions
// switch(fnName){
// case "SetCanvasSize":
// SetCanvasSize(fnParam);
// break;
// }
	
	// https://www.sitepoint.com/call-javascript-function-string-without-using-eval/
	var fn = window[fnName];
	var targetObj = null;
	if (typeof fn === "function"){
		console.log("Vrf Function["+fnName+"] is called with param["+JSON.stringify(fnParam) +"]");
		// fn.apply(window, fnParam);
		setTimeout(function(){
			fn(fnParam);
		},0);
	}else{
		console.log("Vrf Message is ignored since it couldnt find ["+fnName+"] function");
	}
}

function SetCanvasSize(param){
	var wid = param.width;
	var hei = param.height;
	
	var canvas = $('.emscripten#canvas');
	canvas[0].width = wid;
	canvas[0].height = hei;
}


// ////

var Html = "[Html]";
var iframeLevel = 0;
var isUpdateDevPage = true;

console.log('Vrfview.js is loaded');
$(document).ready(function(){
	console.log('Vrfview.js is ready');
})



// /BASICS
function GetVrfIframeWindow(){
	return $('.emscripten#canvas').contentWindow;
}

/*******************************************************************************
 * Connection - init
 ******************************************************************************/
function HtmlOnUnitySceneLoaded(){
	console.log(Html+"OnUnitySceneLoaded is called");
	isUnitySceneLoaded = true;
	
	iframeLevel = 0;
	
	OnUnitySceneLoadedBehaviour();
}

function HtmlOnUnitySceneLoaded(lev){
	console.log(Html+"OnUnitySceneLoaded is called with param(iframe deepness) : "+lev);
	isUnitySceneLoaded = true;

	// iframeLevel does not actually contain level of iframe - it was not
	// possible.
	// values are 1(iframed) or 0(non-iframed)
	iframeLevel = lev;
	
	OnUnitySceneLoadedBehaviour();
}

function OnUnitySceneLoadedBehaviour(){
	console.log("Executing OnUnitySceneLoadedBehaviour");

	// this will lose other thing that is with canvas - loading screen,
	// fullscreen button, etc..
	var otherThanCanvas = $('.template-wrap').find(':not(.emscripten)');
	otherThanCanvas.find('*').css('margin','0');
	otherThanCanvas.remove();
	
	// send to unity(handshake end signal)
	SetIframeLevel(iframeLevel);
	
	// start sending out heartbeat
	var heartbeatPeriod = 5000;// 5sec
	var heartbeatId = window.setInterval(function(){
		// to unity
		SendMessage("[Webgl]ContactPoint","UnityOnHtmlHeartbeat", "(fromjson_Sf)"+Date());
		if(isUpdateDevPage){
			postMessageFunctionFromVrf("HtmlHeartbeat","(fromjson_vrfview)"+Date());	
		}
	}, heartbeatPeriod);
	
	// to host
	if(isUpdateDevPage){
		postMessageFunctionFromVrf("OnUnitySceneLoaded");	
	}
	
	
// if(isUpdateDevPage){
// RelocateUnityCanvas();
// }
}

function SetIframeLevel(lev){
	SendMessage("[Webgl]ContactPoint","UnitySetIframeLevel", lev);
}

/*******************************************************************************
 * Connection
 ******************************************************************************/
function HtmlOnUnityHeartbeat(msg){
	console.log(Html + "FromUnity Heartbeat:" + msg);
}

/*******************************************************************************
 * Character
 ******************************************************************************/
// receives
function HtmlSetPosMinmax(xMin, xMax, yMin, yMax){
	var area = {
			xmin : xMin,
			xmax : xMax,
			ymin : yMin,
			ymax : yMax,
	}
	console.log(Html + "Pos Minmax :" + JSON.stringify(area));
	
	if(isUpdateDevPage){
// SetCharControllableRect(area);
		postMessageFunctionFromVrf(HtmlSetPosMinmax.name,area);	
	}
}

function HtmlSetCharPose(posx,posy,roty){
	var pose = {
			x : posx,
			y : posy,
			rot : roty,
	}
	console.log(Html + "Char Pos :" + JSON.stringify(pose));
	
	if(isUpdateDevPage){
// UpdateCharPose(pose);
		postMessageFunctionFromVrf(HtmlSetCharPose.name,pose);	
	}
}
// sends
function UnitySetCharPose(param){
// {
// x:GetInputgroupText($('#inputboxX')),
// y:GetInputgroupText($('#inputboxY')),
// rot:GetInputgroupText($('#inputboxRot')),
// }
	var x = parseFloat(param.x);
	var y = parseFloat(param.y);
	var rot = parseFloat(param.rot);
	var pose2d = {
			x : x,
			y : y,
			rot : rot,
	}
	var stringified = JSON.stringify(pose2d);
	console.log("UnitySetCharPose - stringifed : "+stringified);
	SendMessage("[Webgl]ContactPoint","UnitySetCharPose", stringified);
}
// postMessageFunctionFromHost(
// "UnitySetCharPose",
// {
// x:GetInputgroupText($('#inputboxX')),
// y:GetInputgroupText($('#inputboxY')),
// rot:GetInputgroupText($('#inputboxRot')),
// }
// );

/*******************************************************************************
 * Camera
 ******************************************************************************/
function HtmlSetCamMode(camMode){
	console.log("HtmlSetCamMode called with param : "+camMode);
	if(isUpdateDevPage){
		postMessageFunctionFromVrf(HtmlSetCamMode.name,{mode:camMode});	
	}
}
function UnitySetCameraMode(param){
	// {
	// mode:"ThirdPerson",FirstPerson
	// }
	SendMessage("[Webgl]ContactPoint","UnitySetCameraMode", param.mode);
}

/*******************************************************************************
 * Corp
 ******************************************************************************/
function HtmlOnNotifyCorpInfos(corpIds, corpNames){
	var len = corpIds.length;
	
	for (i = 0; i < len; i++) {
		var id = corpIds[i];
		var name = corpNames[i];
	    console.log(Html+"Corps : Idx["+i+"] " +"Id["+id+"] " + "Name["+name+"] ");
	}
	
	if(isUpdateDevPage){
// UpdateCharPose(pose);
		postMessageFunctionFromVrf(HtmlOnNotifyCorpInfos.name,{corpIds:corpIds,corpNames:corpNames,});	
	}
	
	// update corp list.
	// you can combine other company info if any, with id as pk.
// if(isUpdateDevPage){
// UpdateCorpsList(pose);
// }
}

function HtmlSetCurrCorp(corpId, corpName){
	console.log(Html+"CurrCorp : "+"Id["+corpId+"] " + "Name["+corpName+"] ");
	var corp = {
		id:corpId,
		name:corpName,
	}
	if(isUpdateDevPage){
// UpdateCurrCorp(corp);
		postMessageFunctionFromVrf(HtmlSetCurrCorp.name,corp);
	}
}

// to unity

function UnityGotoCorpById(param){
// {
// id:"B-02",
// }
	console.log("UnityGotoCorpById called with param "+param.id);
	SendMessage("[Webgl]ContactPoint","UnityGotoCorpWithId", param.id);
}
function UnityGotoCorpByName(param){
// {
// name:"로보스타",
// }
	console.log("UnityGotoCorpByName called with param "+param.name);
	SendMessage("[Webgl]ContactPoint","UnityGotoCorpWithName", param.name);
}


/*******************************************************************************
 * Pano
 ******************************************************************************/
function HtmlSetPanoInfo(strTime, doubleTime, panoWid, panoHei, note){
	console.log(Html+
			"PanoInfo : "+
			"strTime["+strTime+"] " + 
			"doubleTime["+doubleTime+"] " + 
			"panoWid["+panoWid+"] " + 
			"panoHei["+panoHei+"] " +
			"note["+note+"] ");
	
	panoInfo = {
			strTime:strTime,
			doubleTime:doubleTime,
			panoWid:panoWid,
			panoHei:panoHei,
			note:note,
	}
	
	if(isUpdateDevPage){
// UpdateCurrPanoInfo(panoInfo);
		postMessageFunctionFromVrf(HtmlSetPanoInfo.name,panoInfo);
	}
}

/*******************************************************************************
 * Props
 ******************************************************************************/
//call from unity
function HtmlOnNotifyPropInfos(strPropsSerialized){
	/*
	 * input data is such as..
	 * PropMeta[]
	 * [Serializable]
	        public class PropMeta
	        {
	            public string id;
	            public string devNote;
	            public string hoverStr;
	            public bool teleportable;
	        }
		so, parameter as input string
		pMeta : [
	    {
	        "id": "Lg_Main_Hover_KSeries1",
	        "devNote": "K\uc2dc\ub9ac\uc988 \ud0c1\uc790 \uc788\ub294\uacf3",
	        "hoverStr": "K Series",
	        "teleportable": true
	    },]
	 */
	//var strMsg = strPropsSerialized;
	console.log("HtmlOnNotifyPropInfos Received");
	
	var jsonObj = JSON.parse(strPropsSerialized);
	if(!jsonObj.result){
		console.log("Parse reuslt is not good");
	}
	
	console.log("Prop instances count : "+jsonObj.length);
	
	for(var i = 0 ; i < jsonObj.length; i++){
		//unicode to UTF8
		jsonObj[i].hoverStr = unescape(jsonObj[i].hoverStr);
		jsonObj[i].devNote = unescape(jsonObj[i].devNote); 
	}
	
	console.log("strPropsSerialized(UnicodeConverted):" + JSON.stringify(jsonObj));
}

//called every ~ seconds
function HtmlSetCurrProp(propId, devNote, hoverStr, teleportable){
	
	console.log("HtmlSetCurrProp Received with param : "+propId);
	console.log(Html+"CurrProp : "+"Id["+propId+"] " + "hoverStr["+hoverStr+"] ");
	var prop = {
			propId:propId,
			devNote:devNote,
			hoverStr:hoverStr,
			teleportable:teleportable,
	}
	if(isUpdateDevPage){
// UpdateCurrCorp(corp);
		postMessageFunctionFromVrf(HtmlSetCurrProp.name,prop);
	}
}


//click action
function HtmlOnClickPropWithId (strId){
	//var strMsg = strPropsSerialized;
	console.log("HtmlOnClickPropWithId Received with param : "+strId);
	var propId = {
			id:strId,
	}
	if(isUpdateDevPage){
		postMessageFunctionFromVrf(HtmlOnClickPropWithId.name,propId);
	}
}

//call to unity
function UnityGotoPropById(param){
// {
// id:"LG_Main_Seriesx1",
// }
	console.log("UnityGotoPropWithId called with param "+param.id);
	SendMessage("[Webgl]ContactPoint","UnityGotoPropWithId", param.id);
}


