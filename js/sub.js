$(document).ready(function () {
    $('.info02_over').css({
        rotateY: '180deg'
    })
    $('#company_info02 li').hover(function () {
     
        $('.info02_out', this).animate({
            rotateY: '180deg',
            opacity: '0'
        }, 300);
        $('.info02_over', this).animate({
            rotateY: '0deg',
            opacity: '1'
        }, 300);
        
    }, function () {
            $('.info02_out', this).animate({
                rotateY: '0deg',
                opacity: '1'
            }, 300);
            $('.info02_over', this).animate({
                rotateY: '180deg',
                opacity: '0'
            }, 300)
  
       
    });
    
})