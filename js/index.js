﻿$(document).ready(function () {
    
    // 마우스 올렸을때 버튼 색상 변경
    $('.go_btn').hover(function () {
        $('.go_out',this).css('display', 'none');
        $('.go_over',this).css('display', 'block');
    }, function () {
        $('.go_out',this).css('display', 'block');
        $('.go_over',this).css('display', 'none');
    })

    $('.arrow').hover(function () {
        $('.arrow_over', this).css('display', 'block');
    }, function () {
        $('.arrow_over', this).css('display', 'none');
    })
    

    

    // 베타 테스터 모집 목록 움직임
    var now = 0;
    var con_width = $('#beta_img li').width()+14;
    var max = 6;

    function move() {
        var a = -con_width * now;
        $('#beta_img ul').stop().animate({
            left: a
        }, 500)
    };
    $('#arrow_left').click(function () {
        if (now > 0) {
            now--;
            move();
        }
    })
    $('#arrow_right').click(function () {
        if (now < max - 4) {
            now++;
            move();
        }
    })
       
})